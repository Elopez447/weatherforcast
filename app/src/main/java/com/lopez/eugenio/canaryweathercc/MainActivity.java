package com.lopez.eugenio.canaryweathercc;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.lopez.eugenio.canaryweathercc.ViewModel.MainActivityViewModel;
import com.lopez.eugenio.canaryweathercc.adapter.ForecastAdapter;
import com.lopez.eugenio.canaryweathercc.adapter.HourlyAdapter;
import com.lopez.eugenio.canaryweathercc.databinding.ActivityMainBinding;
import com.lopez.eugenio.canaryweathercc.model.Forecast;
import com.lopez.eugenio.canaryweathercc.network.ApiResponseCallback;

public class MainActivity extends AppCompatActivity implements ApiResponseCallback {

    //TODO implement google places for auto complete searches
    //TODO add a view pager to dynamically add and remove fragments
    //TODO sync the wweather app with googel calander to suggest what kind of weather you'll have during an upcoming outing.
    //TODO create helper class for getDrawable
    //TODO fix RecyclerView widths and heights
    //TODO figure out how to get data faster or add a splash screen
    //TODO add swipe to refresh
    //TODO add layout for landscape mode


    //Accomplished for v.0.2
    // changed the in apps status and bottome nav bar to be translucent

    ActivityMainBinding activityMainBinding;
    private Toolbar toolbar;
    private SearchView searchView;
    private Menu menu;
    private RecyclerView recyclerView;
    private RecyclerView hourlyRecyclerView;
    private MainActivityViewModel mainActivityViewModel;
    private int REQUEST_LOCATION_PERMISSION_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainActivityViewModel = new MainActivityViewModel(this, this, MainActivity.this);
        mainActivityViewModel.checkLocationPermissions();
//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_COARSE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
//                    1);
//        }
        toolbar = activityMainBinding.toolbar;
        setSupportActionBar(toolbar);

        initRecyclerView(activityMainBinding.recyclerView);
        initHourlyRecyclerView(activityMainBinding.hourlyRecyclerView);
        mainActivityViewModel.getLocation();
        mainActivityViewModel.geoLocate();
        activityMainBinding.setViewModel(mainActivityViewModel);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (REQUEST_LOCATION_PERMISSION_CODE == 10) {
            mainActivityViewModel = new MainActivityViewModel(this, this, MainActivity.this );
            mainActivityViewModel.getLocation();
        }
    }


    public void initRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void initHourlyRecyclerView(RecyclerView recyclerView) {
        this.hourlyRecyclerView = recyclerView;
        this.hourlyRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void onSuccess(Forecast response) {

        activityMainBinding.setModel(response.daily.getData().get(0));
        activityMainBinding.setCurrentlyModel(response.currently);
        activityMainBinding.setHourlyModel(response.hourly);

        hourlyRecyclerView.setAdapter(new HourlyAdapter(this, this, response.hourly.getData()));
        recyclerView.setAdapter(new ForecastAdapter(this, response.daily.getData(), this));
    }

    @Override
    public void onCity(String city) {
        activityMainBinding.location.setText(city);
    }

    @Override
    public void onError(Throwable t) {
        Log.v("MAIN", "error response:" + t.toString());
    }












}
