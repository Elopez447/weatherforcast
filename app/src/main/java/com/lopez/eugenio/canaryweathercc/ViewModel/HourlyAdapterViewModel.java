package com.lopez.eugenio.canaryweathercc.ViewModel;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.lopez.eugenio.canaryweathercc.R;
import com.lopez.eugenio.canaryweathercc.model.Data;
import com.lopez.eugenio.canaryweathercc.network.ApiResponseCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Eugenio on 7/27/18.
 */

public class HourlyAdapterViewModel {

    private Context mContext;

    public HourlyAdapterViewModel(Context context, ApiResponseCallback responseListener) {
        this.mContext = context;
    }

    public String getTime(Data data){

        Date date = new Date(data.getTime() * 1000L);
        DateFormat dateFormat = new SimpleDateFormat("ha");
        String formatted = dateFormat.format(date);

        return formatted;
    }

    public Drawable getDrawable(Data data){

        Drawable drawable = null;
        if (data != null) {

            Log.d("DATA ICON", data.getIcon());

            switch (data.getIcon()) {
                case "clear-day":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.sunny);
                    break;
                case "partly-cloudy-day":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.partlycloudy);
                    break;
                case "rain":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.rain);
                    break;
                case "clear-night":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.nt_clear);
                    break;
                case "snow":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.snow);
                    break;
                case "sleet":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.sleet);
                    break;
                case "wind":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.wind);
                    break;
                case "fog":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.fog);
                    break;
                case "cloudy":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.cloudy);
                    break;
                case "partly-cloudy-night":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.nt_partlycloudy);
                    break;
                case "hail":  //use sleet icon
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.sleet);
                    break;
                case "thunderstorm":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.tstorms);
                    break;
            }
        } else {
            drawable = ContextCompat.getDrawable(mContext, R.drawable.unknown);
        }

        return drawable;
    }

}
