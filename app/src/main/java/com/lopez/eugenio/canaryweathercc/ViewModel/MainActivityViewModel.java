package com.lopez.eugenio.canaryweathercc.ViewModel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.lopez.eugenio.canaryweathercc.R;
import com.lopez.eugenio.canaryweathercc.model.Currently;
import com.lopez.eugenio.canaryweathercc.network.ApiFactory;
import com.lopez.eugenio.canaryweathercc.network.ApiResponseCallback;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Eugenio on 7/21/18.
 */

public class MainActivityViewModel {

    private ApiFactory apiFactory;
    private ApiResponseCallback responseListener;
    private Context mContext;
    private Activity activity;

    public MainActivityViewModel(Context context, ApiResponseCallback responseListener, Activity activity) {
        this.mContext = context;
        this.activity = activity;
        this.responseListener = responseListener;
        this.apiFactory = new ApiFactory(mContext, responseListener);
    }


    public void getLocation() {

        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            //handled in main activity
            return;
        }

        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener((Activity) mContext, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {

                        if (location != null) {

                            double latitude = location.getLatitude();
                            double longitude = location.getLongitude();

                            String mCoordinates = String.valueOf(latitude)
                                    + "," + String.valueOf(longitude);

                            String city = String.valueOf(getCity(latitude, longitude));

                            // passes down city/town name to mainActivity
                            responseListener.onCity(city);

                            apiFactory.getForecast(mCoordinates);

                        } else {
                            responseListener.onError(new Throwable("Location is null"));
                        }

                    }
                });
    }

    public String getCity(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mContext, Locale.getDefault());

        try {

            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address address = addresses.get(0);
            return address.getLocality();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void geoLocate(){

        //does nothing now until user submits input

        String locationName = "Jersey City";

        Geocoder geocoder = new Geocoder(mContext);
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(locationName,1);

            if(addresses.size() >= 0){
                Log.d("GEO LOCATE Latitude =", String.valueOf(addresses.get(0).getLatitude()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void checkLocationPermissions(){
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }
    }








    //called within activity_main.xml
    //since same method exist in forecastLVM, a helper class may be needed if redundant code continues to appear.
    public Drawable getDrawable(Currently data) {
        Drawable drawable = null;

        if (data != null) {

            Log.d("DATA ICON", data.getIcon());

            switch (data.getIcon()) {
                case "clear-day":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.sunny);
                    break;
                case "partly-cloudy-day":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.partlycloudy);
                    break;
                case "rain":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.rain);
                    break;
                case "clear-night":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.nt_clear);
                    break;
                case "snow":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.snow);
                    break;
                case "sleet":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.sleet);
                    break;
                case "wind":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.wind);
                    break;
                case "fog":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.fog);
                    break;
                case "cloudy":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.cloudy);
                    break;
                case "partly-cloudy-night":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.nt_partlycloudy);
                    break;
                case "hail":  //use sleet icon
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.sleet);
                    break;
                case "thunderstorm":
                    drawable = ContextCompat.getDrawable(mContext, R.drawable.tstorms);
                    break;
            }
        } else {
            drawable = ContextCompat.getDrawable(mContext, R.drawable.unknown);
        }

        return drawable;
    }

}
