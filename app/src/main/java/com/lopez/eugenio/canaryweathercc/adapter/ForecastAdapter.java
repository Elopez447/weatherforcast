package com.lopez.eugenio.canaryweathercc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lopez.eugenio.canaryweathercc.ViewModel.ForecastListViewModel;
import com.lopez.eugenio.canaryweathercc.databinding.ViewItemBinding;
import com.lopez.eugenio.canaryweathercc.model.Data;
import com.lopez.eugenio.canaryweathercc.network.ApiResponseCallback;

import java.util.ArrayList;

/**
 * Created by Eugenio on 6/10/18.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {
    private ArrayList<Data> data;
    private Context context;
    private ApiResponseCallback apiResponseCallback;

    public ForecastAdapter(Context con, ArrayList<Data> data, ApiResponseCallback apiResponseCallback) {
        this.context = con;
        this.data = data;
        this.apiResponseCallback = apiResponseCallback;
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewItemBinding itemBinding = ViewItemBinding.inflate(layoutInflater, parent, false);

        return new ForecastViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {

        Data data = getItemForPosition(position);
        holder.bind(data, context);

    }

    public Data getItemForPosition(int position) {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ForecastViewHolder extends RecyclerView.ViewHolder {
        private final ViewItemBinding binding;
        ImageView icon;


        public ForecastViewHolder(ViewItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            icon = binding.imageIcon;
        }

        public void bind(Data data, Context context) {
            binding.setModel(data);
            binding.setViewModel(new ForecastListViewModel(context, apiResponseCallback));
            binding.executePendingBindings();
        }

    }
}
