package com.lopez.eugenio.canaryweathercc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lopez.eugenio.canaryweathercc.ViewModel.HourlyAdapterViewModel;
import com.lopez.eugenio.canaryweathercc.databinding.HourlyViewItemBinding;
import com.lopez.eugenio.canaryweathercc.model.Data;
import com.lopez.eugenio.canaryweathercc.network.ApiResponseCallback;

import java.util.ArrayList;

/**
 * Created by Eugenio on 7/27/18.
 */

public class HourlyAdapter extends RecyclerView.Adapter<HourlyAdapter.HourlyViewHolder>{

    private Context context;
    private ApiResponseCallback apiResponseCallback;
    private ArrayList<Data> data;

    public HourlyAdapter(Context context, ApiResponseCallback apiResponseCallback, ArrayList<Data> data){
        this.data = data;
        this.context = context;
        this.apiResponseCallback = apiResponseCallback;
    }

    @Override
    public HourlyAdapter.HourlyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        HourlyViewItemBinding hourlyViewItemBinding = HourlyViewItemBinding.inflate(layoutInflater,parent,false);

        return new HourlyViewHolder(hourlyViewItemBinding);
    }

    @Override
    public void onBindViewHolder(HourlyViewHolder holder, int position) {

        Data data = getItemForPosition(position);
        holder.bind(data,context);

    }

    public Data getItemForPosition(int position) {
        return data.get(position);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public class HourlyViewHolder extends RecyclerView.ViewHolder {
        private final HourlyViewItemBinding binding;
        ImageView icon;

        public HourlyViewHolder(HourlyViewItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            icon = binding.icon;
        }

        public void bind(Data data, Context context){
            binding.setHourlyModel(data);
            binding.setViewModel(new HourlyAdapterViewModel(context, apiResponseCallback));
            binding.executePendingBindings();
        }
    }
}












