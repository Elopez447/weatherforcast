package com.lopez.eugenio.canaryweathercc.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Eugenio on 7/26/18.
 */

public class Currently {

    private Long time;
    private String summary;
    private String icon;
    private Double precipProbability;
    private Double temperature;
    private Double apparentTemperature;
    private String precipType;



    public String getTime() {

        Date date = new Date(time * 1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM, dd");
        String formatted = simpleDateFormat.format(date);
        return String.valueOf(formatted);
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Double getPrecipProbability() {
        return precipProbability;
    }

    public void setPrecipProbability(Double precipProbability) {
        this.precipProbability = precipProbability;
    }

    public String getTemperature() {
        return String.valueOf(Math.round(temperature)) + "°";
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getApparentTemperature() {
        return String.valueOf(Math.round(apparentTemperature)) + "°";
    }

    public void setApparentTemperature(double apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
    }

    public String getPrecipType() {
        return precipType;
    }

    public void setPrecipType(String precipType) {
        this.precipType = precipType;
    }

}
