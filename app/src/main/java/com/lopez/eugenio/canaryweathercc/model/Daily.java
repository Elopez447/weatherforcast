package com.lopez.eugenio.canaryweathercc.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Eugenio on 6/8/18.
 */


public class Daily {

    @SerializedName("summary")
    private String summary;
    @SerializedName("data")
    private ArrayList<Data> data;

    public Daily(){

    }


    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}


