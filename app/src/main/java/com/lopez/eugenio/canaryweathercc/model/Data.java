package com.lopez.eugenio.canaryweathercc.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Eugenio on 6/08/18.
 */

public class Data implements Parcelable {

    public Data() {

    }


    @SerializedName("time")
    private Long time;
    @SerializedName("summary")
    private String summary;
    @SerializedName("icon")
    private String icon;
    @SerializedName("precipType")
    private String precipType;
    @SerializedName("precipProbability")
    private Double precipProbability;
    @SerializedName("temperatureHigh")
    private Double temperatureHigh;
    @SerializedName("temperatureLow")
    private Double temperatureLow;
    @SerializedName("apparentTemperature")
    private Double apparentTemperature;
    @SerializedName("temperature")
    private Double temperature;


    protected Data(Parcel in) {
        summary = in.readString();
        icon = in.readString();
        precipType = in.readString();

        if (in.readByte() == 0) {
            time = null;
        } else {
            time = in.readLong();
        }

        if (in.readByte() == 0) {
            temperatureHigh = null;
        } else {
            temperatureHigh = in.readDouble();
        }
        if (in.readByte() == 0) {
            temperature = null;
        } else {
            temperature = in.readDouble();
        }
        if (in.readByte() == 0) {
            temperatureLow = null;
        } else {
            temperatureLow = in.readDouble();
        }
        if (in.readByte() == 0) {
            apparentTemperature = null;
        } else {
            apparentTemperature = in.readDouble();
        }
        if (in.readByte() == 0) {
            precipProbability = null;
        } else {
            precipProbability = in.readDouble();
        }
    }



    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPrecipType() {
        return precipType;
    }

    public void setPrecipType(String precipType) {
        this.precipType = precipType;
    }

    public String getTemperatureHigh() {
        return String.valueOf(Math.round(temperatureHigh)) + "°";
    }

    public void setTemperatureHigh(String temperatureHigh) {
        this.temperatureHigh = Double.valueOf(temperatureHigh);
    }


    public String getTemperatureLow() {
        return "/" + String.valueOf(Math.round(temperatureLow)) + "°";
    }

    public void setTemperatureLow(String temperatureLow) {

        this.temperatureLow = Double.valueOf(temperatureLow);
    }

    public String getApparentTemperature() {
        return String.valueOf(Math.round(apparentTemperature)) + "°";
    }

    public void setApparentTemperature(String apparantTemperature) {
        this.apparentTemperature = Double.valueOf(apparantTemperature);
    }


    public String getPrecipProbability() {
        precipProbability = 100 * precipProbability;
        return String.valueOf(Math.round(precipProbability)) + "%";
    }

    public void setPrecipProbability(String precipProbability) {
        this.precipProbability = Double.valueOf(precipProbability);
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }


    public Long getTime() {
//
//        Date date = new Date(time * 1000L);
//        DateFormat format = new SimpleDateFormat("EEE, MMM dd");
//        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
//        String formatted = format.format(date);
//
        return time;
    }

    public void setTime(Long time) {


        this.time = time;
    }

    public String getTemperature() {
        return String.valueOf(Math.round(temperature)) + "°";
    }

    public void setTemperature(Double temperature) {
        this.temperatureHigh = Double.valueOf(temperature);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(summary);
        dest.writeString(icon);
        dest.writeString(precipType);

        if (time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(time);
        }
        if (temperature == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temperature);
        }
        if (temperatureHigh == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temperatureHigh);
        }
        if (temperatureLow == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temperatureLow);
        }
        if (apparentTemperature == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(apparentTemperature);
        }
        if (precipProbability == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(precipProbability);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };


}
