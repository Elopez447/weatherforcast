package com.lopez.eugenio.canaryweathercc.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Eugenio on 6/8/18.
 */

public class Forecast implements Parcelable {

    @SerializedName("timezone")
    private String timezone;
    @SerializedName("daily")
    public Daily daily;
    @SerializedName("currently")
    public Currently currently;
    @SerializedName("hourly")
    public Hourly hourly;



    public Forecast() {

    }


    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Daily getDaily() {
        return daily;
    }

    public void setDaily(Daily daily) {
        this.daily = daily;
    }

    public Currently getCurrently() {
        return currently;
    }

    public void setCurrently(Currently currently) {
        this.currently = currently;
    }

    public Hourly getHourly() {
        return hourly;
    }

    public void setHourly(Hourly hourly) {
        this.hourly = hourly;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(timezone);
    }

    protected Forecast(Parcel in) {
        timezone = in.readString();
    }

    public static final Creator<Forecast> CREATOR = new Creator<Forecast>() {
        @Override
        public Forecast createFromParcel(Parcel in) {
            return new Forecast(in);
        }

        @Override
        public Forecast[] newArray(int size) {
            return new Forecast[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }
}