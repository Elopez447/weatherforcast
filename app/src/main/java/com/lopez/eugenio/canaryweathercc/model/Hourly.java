package com.lopez.eugenio.canaryweathercc.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Eugenio on 7/26/18.
 */

public class Hourly {

    @SerializedName("data")
    private ArrayList<Data> data;

    public Hourly(){

    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

}
