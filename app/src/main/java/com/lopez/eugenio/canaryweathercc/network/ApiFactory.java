package com.lopez.eugenio.canaryweathercc.network;

import android.content.Context;

import com.lopez.eugenio.canaryweathercc.R;
import com.lopez.eugenio.canaryweathercc.model.Forecast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Eugenio on 6/8/18.
 */

public class ApiFactory implements Callback<Forecast>  {
    private Context context;
    private static Retrofit retrofit = null;
    private ForecastInterface forecastApi;
    private ApiResponseCallback apiResponseCallback;

    public ApiFactory(Context context, ApiResponseCallback callback){
        this.context = context;
        this.apiResponseCallback = callback;
        forecastApi = getClient().create(ForecastInterface.class);
    }

    private Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(context.getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


    public void getForecast(String mCoordinates) {
        final Call<Forecast> call = forecastApi.getForecast(context.getResources().getString(R.string.api_key),mCoordinates);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<Forecast> call, Response<Forecast> response) {
        apiResponseCallback.onSuccess(response.body());
    }


    @Override
    public void onFailure(Call<Forecast> call, Throwable t) {
        apiResponseCallback.onError(t);
    }


}
