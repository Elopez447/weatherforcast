package com.lopez.eugenio.canaryweathercc.network;

import com.lopez.eugenio.canaryweathercc.model.Forecast;

/**
 * Created by Eugenio on 6/8/18.
 */

public interface ApiResponseCallback {

    void onSuccess(Forecast response);
    void onCity(String city);
    void onError(Throwable t);

}
