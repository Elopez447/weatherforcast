package com.lopez.eugenio.canaryweathercc.network;

import com.lopez.eugenio.canaryweathercc.model.Forecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Eugenio on 6/8/18.
 */

public interface ForecastInterface {

    @GET("forecast/{apiKey}/{coordinates}")
    Call<Forecast> getForecast(@Path("apiKey") String apiKey, @Path("coordinates")String coordinates);

}
